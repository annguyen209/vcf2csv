﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using vCardLib;
using vCardLib.Collections;

namespace ReadVCF
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtBrowse.Text = openFileDialog1.FileName;
                ReadVcfFile(openFileDialog1.FileName);
            }
        }

        private void ReadVcfFile(string filePath)
        {
            vCardCollection contacts = vCardLib.Deserializers.Deserializer.FromFile(filePath);
            var mainContacts = new List<MainContact>();
            foreach (var item in contacts)
            {
                var vCardItem = (vCard)item;
                var newMainContact = new MainContact();
                var fullname = vCardItem.FormattedName.Trim();
                // Extract name
                string pattern = @"^\[[\w\s-]+\][\w\s-]+\([\w\s]+\)[\w\s.]+$";
                if(!Regex.IsMatch(fullname, pattern))
                {
                    continue;
                }

                newMainContact.FullName = fullname;

                var splitNameArr = fullname.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                var name = string.Format("Enc {0} {1}", splitNameArr[1], splitNameArr[2].Replace("(", "").Replace(")", ""));
                newMainContact.Mood = vCardItem.XSkypeMood;
                newMainContact.Name = name;
                if (vCardItem.PhoneNumbers.Count > 0)
                {
                    newMainContact.Phone = vCardItem.PhoneNumbers[0].Number;
                }
                else
                {
                    if (newMainContact.Mood.Contains("Mobile"))
                    {
                        var extractedPhone = string.Join("", newMainContact.Mood.ToCharArray().Where(char.IsDigit));
                        if (!string.IsNullOrEmpty(extractedPhone))
                        {
                            newMainContact.Phone = "+" + extractedPhone;
                            newMainContact.PhoneExtractedFromMood = true;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(newMainContact.Phone))
                {
                    mainContacts.Add(newMainContact);
                }
            }
            dataGridView1.DataSource = mainContacts;
            


        }

        private void btnExportAllCSV_Click(object sender, EventArgs e)
        {
            if(dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("Load data first");
                return;
            }
            if (dataGridView1.SelectedRows.Count == 0)
            {
                dataGridView1.SelectAll();
            }
            var result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                var filePath = saveFileDialog1.FileName;
                var builder = new StringBuilder();
                builder.Append("Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Group Membership,E-mail 1 - Type,E-mail 1 - Value,E-mail 2 - Type,E-mail 2 - Value,Phone 1 - Type,Phone 1 - Value,Phone 2 - Type,Phone 2 - Value,Website 1 - Type,Website 1 - Value");
                builder.AppendLine();
                foreach (DataGridViewRow item in dataGridView1.SelectedRows)
                {
                    builder.AppendLine(string.Format("{0},,,,,,,,,,,,,,,,,,,,,,,,,,* My Contacts,,,,,Mobile,{1},,,,", item.Cells["Name"].Value, item.Cells["Phone"].Value));
                }
                File.WriteAllText(filePath, builder.ToString());
                MessageBox.Show("Done");
            }
        }

        private void btnExportSelected_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("Load data first");
                return;
            }
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Select at least one record");
                return;
            }
            var result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                var filePath = saveFileDialog1.FileName;
                var builder = new StringBuilder();
                builder.Append("Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Group Membership,E-mail 1 - Type,E-mail 1 - Value,E-mail 2 - Type,E-mail 2 - Value,Phone 1 - Type,Phone 1 - Value,Phone 2 - Type,Phone 2 - Value,Website 1 - Type,Website 1 - Value");
                builder.AppendLine();
                foreach (DataGridViewRow item in dataGridView1.SelectedRows)
                {
                    builder.AppendLine(string.Format("{0},,,,,,,,,,,,,,,,,,,,,,,,,,* My Contacts,,,,,Mobile,{1},,,,", item.Cells["Name"].Value, item.Cells["Phone"].Value));
                }
                File.WriteAllText(filePath, builder.ToString());
                MessageBox.Show("Done");
            }
        }

        private void btnExportNoneSelect_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("Load data first");
                return;
            }
            if (dataGridView1.SelectedRows.Count == dataGridView1.Rows.Count)
            {
                MessageBox.Show("All records was selected");
                return;
            }
            var result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                var filePath = saveFileDialog1.FileName;
                var builder = new StringBuilder();
                builder.Append("Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Group Membership,E-mail 1 - Type,E-mail 1 - Value,E-mail 2 - Type,E-mail 2 - Value,Phone 1 - Type,Phone 1 - Value,Phone 2 - Type,Phone 2 - Value,Website 1 - Type,Website 1 - Value");
                builder.AppendLine();
                foreach (DataGridViewRow item in dataGridView1.Rows)
                {
                    if (!item.Selected)
                    {
                        builder.AppendLine(string.Format("{0},,,,,,,,,,,,,,,,,,,,,,,,,,* My Contacts,,,,,Mobile,{1},,,,", item.Cells["Name"].Value, item.Cells["Phone"].Value));
                    }
                }
                File.WriteAllText(filePath, builder.ToString());
                MessageBox.Show("Done");
            }
        }

        private void btnFixMe_Click(object sender, EventArgs e)
        {
            DialogResult openResult = openFileDialog1.ShowDialog();
            if (openResult == DialogResult.OK)
            {
                var fileToFixPath = openFileDialog1.FileName;                

                var saveResult = saveFileDialog1.ShowDialog();
                if (saveResult == DialogResult.OK)
                {
                    var filePath = saveFileDialog1.FileName;
                    var builder = new StringBuilder();
                    builder.Append("Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Group Membership,E-mail 1 - Type,E-mail 1 - Value,E-mail 2 - Type,E-mail 2 - Value,Phone 1 - Type,Phone 1 - Value,Phone 2 - Type,Phone 2 - Value,Website 1 - Type,Website 1 - Value");
                    builder.AppendLine();
                    StreamReader file = new StreamReader(fileToFixPath);
                    string line;
                    var isFirstLine = true;
                    while ((line = file.ReadLine()) != null)
                    {
                        if (isFirstLine)
                        {
                            isFirstLine = false;
                            continue;
                        }
                        var splitStr = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        builder.AppendLine(string.Format("{0},,,,,,,,,,,,,,,,,,,,,,,,,,* My Contacts,,,,,Mobile,{1},,,,", splitStr[0], splitStr[2]));
                    }

                    file.Close();
                    
                    File.WriteAllText(filePath, builder.ToString(), Encoding.Unicode);
                    MessageBox.Show("Done");
                }
            }
        }
    }
}
