﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadVCF
{
    public class MainContact
    {
        public string FullName { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Mood { get; set; }
        public bool PhoneExtractedFromMood { get; set; }
    }
}
