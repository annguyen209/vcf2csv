﻿namespace vCardLib.Models
{
    public class Geo
    {
        /// <summary>
        /// The longitude of the location
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// The latitude of the location
        /// </summary>
        public double Latitude { get; set; }
    }
}
